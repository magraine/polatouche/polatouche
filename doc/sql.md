# Accès SQL

## MySQL

En local, avec Mamp ou Mamp Pro, le socket de connexion à mysql n’est pas déclaré en CLI 
et utilise le chemin par défaut, et non le chemin de Mamp.
Pour régler cela, voici une solution : 

    sudo mkdir /var/mysql
    sudo ln -s /Applications/MAMP/tmp/mysql/mysql.sock

## SQLite

En local il est possible que la BDD soit accessible uniquement en lecture par l’utilisateur CLI,
si celui-ci est différent de l’utilisateur apache. C'est le cas avec Mamp.

    cd config/bases/
    sudo chmod 666 spip.sqlite

