# Les commandes

Quelques commandes sont fornies dans le paquet.

## Test

Tester le démarrage de SPIP et l’accès à la base de données.

    polatouche test:spip
  
   
  
## Webmestre observateur

Créer un webmestre d’identifiant -1 (pour connexion temporaire à un site).

    polatouche spip:webmestre
    
La commande affichera un login et un mot de passe de connexion tel que :

    ! [NOTE] Login : Polatouche-5d168054
    ! [NOTE] Password : 7739f672807c946880c5a4b2262d8f224

Le paramètre `-f` force une recréation si ce webmestre existe déjà.

    polatouche spip:webmestre -f

Pour supprimer ce webmestre

    polatouche spip:webmestre -d

