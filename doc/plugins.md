# Gestion des plugins

Se référer à Cilex ou Silex pour le chargement de Service. Il en existe déjà un certain nombre.

En sus, il est possible de charger des Services spéciaux pour Polatouches nommés Plugins.
Il est dans ce cas conseillé d’implémenter `Polatouche\Plugin\ProviderInterface` plutôt que 
`Pimple\ServiceProviderInterface`. Cela permet d’effectuer des actions automatiques
lors de l’appel à `register()`, notamment pour déclarer automatiquement les commandes
et les traductions du plugins, à partir du moment où elles sont rangées respectivement
dans `Command\xxxCommand.php` et `Resources\translations\xxx.fr_FR.php` pour les traductions.

## Pour charger un plugin :

    ```php
    use Polatouche\Application;
    $app = new Application();
    $app->register(new Polatouche\Plugin\Faker\FakerPlugin());
    $app->run();
    ```

## Pour obtenir un service dans une commande :

    ```php
    // utiliser getService dans la fonction Execute.
    $translator = $this->getService('translator');
    
    // ou sinon obtenir directement l’application
    $app = $this->getApplication();
    $translator = $app['translator'];
    ```

## composer.json et plugins

Il est possible de faire charger automatiquement un plugin s’il a été requis par un composer.json.
Dans le composer.json du plugin, indiquer `"type": "polatouche-plugin",` fera comprendre
à Composer lors de l’installation (via le paquet `polatouche/composer-installer` installé avec Polatouche.), que ce paquet doit s’installer dans le répertoire `plugins/` (et non dans `vendor/`).

Tous les plugins présents dans ce répertoire sont activés par défaut au chargement de Polatouche
via la méthode `autoRegisterPlugins()`. 
Cette auto activation est débrayable si besoin avec l’option `polatouche.auto-register-plugins` à `false`.

    ```php
    $app = new Application([
        'polatouche.auto-register-plugins' => false,
    ]);
    $app->run();
    ```
