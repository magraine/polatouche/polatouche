# Description

Polatouche est un outil de ligne de commande pour SPIP.
Il utilise [Cilex](https://github.com/Cilex/Cilex) 
(et donc l’injecteur de dépendance Pimple et la Console Symfony)

Cet outil est présent pour l’instant **à des fins expérimentales uniquement**.

Voir 'Polatouche Tout' pour une installation avec les plugins.

## Installation 

    cd ~/scripts
    git clone git@gitlab.com:magraine/polatouche/polatouche.git polatouche
    cd polatouche
    composer install
    cd vendor/bin
    ln -s $(pwd)/polatouche /usr/local/bin/

### Auto-Complétion

#### Sous Linux

    ln -s $(pwd)/polatouche_console_autocomplete /etc/bash_completion.d/polatouche
    
#### Sous macOs

    ln -s $(pwd)/polatouche_console_autocomplete /usr/local/etc/bash_completion.d/polatouche

# Documentation

Se reporter au répertoire doc/ pour plus d’informations.
