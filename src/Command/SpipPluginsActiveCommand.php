<?php

namespace Polatouche\Command;

use Polatouche\Provider\Console\Style\PolatoucheStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SpipPluginsActiveCommand extends SpipPluginsListCommand
{

	protected $todo = [];

	protected function configure() {
		$this->setName("spip:plugins:activate")
			->setDescription("Active un ou plusieurs plugins")
			->addArgument('from', InputArgument::OPTIONAL|InputArgument::IS_ARRAY, 'Active les plugins listés. Détermine automatiquement l’option from-xxx.')
			->addOption('from-file', null, InputOption::VALUE_OPTIONAL, 'Chemin d’un fichier d’export')
			->addOption('from-url', null, InputOption::VALUE_OPTIONAL, 'Url d’un site SPIP')
			->addOption('from-list', null, InputOption::VALUE_OPTIONAL, 'Liste de préfixes à activer, séparés par virgule')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = new PolatoucheStyle($input, $output);
		$this->io->title("Activer des plugins");

		$spip = $this->getService('spip.loader');
		$spip->load();

		if ($input->getOption('from-file')) {
			$this->addTodo($this->getPrefixesFromFile($input->getOption('from-file')));
		} elseif ($input->getOption('from-url')) {
			$this->addTodo($this->getPrefixesFromUrl($input->getOption('from-url')));
		} elseif ($input->getOption('from-list')) {
			$this->addTodo(explode(',', $input->getOption('from-list')));
		}

		$from = $input->getArgument('from');
		foreach ($from as $quoi) {
			if (preg_match(',^https?://,', $quoi)) {
				$this->addTodo($this->getPrefixesFromUrl($quoi));
			} elseif (strpbrk($quoi, '.\\/')) {
				$this->addTodo($this->getPrefixesFromFile($quoi));
			} else {
				$this->addTodo([$quoi]);
			}
		}

		if (!$liste = $this->getTodo()) {
			$this->io->care("Aucun prefixe à activer");
			return;
		}
		$this->io->text("Liste des plugins à activer :");
		$this->presenterListe($liste);

		$this->actualiserPlugins();
		$this->activePlugins($liste);
	}

	public function getPrefixesFromFile($file) {
		if (!is_file($file)) {
			throw new \Exception("File doesn't exists : " . $file);
		}
		$list = file_get_contents($file);
		return explode(' ', $list);
	}

	public function getPrefixesFromUrl($url) {
		// si on a un fichier local/config.txt on le prend en priorite
		exec("curl -L --silent $url/local/config.txt",$head);
		$head = implode("\n", $head)."\n";
		if (!preg_match(",^Composed-By:(.*)\n,Uims", $head, $m)) {
			exec("curl -I -L --silent $url", $head);
			$head = implode("\n", $head);
		}
		if (preg_match(",^Composed-By:(.*)\n,Uims", $head, $m)) {
			// virer les numeros de version
			$liste = preg_replace(",\([^)]+\),", "", $m[1]);
			$liste = explode(",", $liste);
			$liste = array_map('trim', $liste);
			array_shift($liste);
		}
		return $liste;
	}

	public function addTodo(array $prefixes) {
		$prefixes = array_map('trim', $prefixes);
		$prefixes = array_map('strtolower', $prefixes);
		$this->todo = array_unique(array_merge($this->todo, $prefixes));
	}

	public function getTodo() {
		return $this->todo;
	}

	public function activePlugins($prefixes) {
		if (!count($prefixes)) {
			$this->io->care("Aucun prefixe à activer");
			return true;
		}
		$actifs = array_keys($this->getPluginsActifs());
		$actifs = array_map('strtolower', $actifs);

		if ($deja = array_intersect($actifs, $prefixes)) {
			$prefixes = array_diff($prefixes, $actifs);
			if ($prefixes) {
				$this->io->text("Certains préfixes demandés sont déjà actifs :");
				$this->presenterListe($deja);
			} else {
				$this->io->check("Tous les préfixes demandés sont déjà actifs");
				return true;
			}
		}

		$inactifs = $this->getPluginsInactifs();
		$activer = [];
		foreach ($inactifs as $plugin) {
			$prefixe = strtolower($plugin['nom']);
			if (in_array($prefixe, $prefixes)) {
				$activer[] = $plugin['dir'];
				$prefixes = array_diff($prefixes, [$prefixe]);
			}
		}

		if (count($prefixes)) {
			$this->io->fail("Certains préfixes demandés sont introuvables :");
			$this->presenterListe($prefixes);
		}

		if (count($activer)) {
			ecrire_plugin_actifs($activer, false, 'ajoute');
			$actifs = $this->getPluginsActifs(['procure' => false, 'php' => false]);
			$this->io->text("Plugins actifs après action :");
			$this->showPlugins($actifs);
		}

	}

}