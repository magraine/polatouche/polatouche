<?php

namespace Polatouche\Command;

use Polatouche\Provider\Console\Style\PolatoucheStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SpipPluginsDesactiveCommand extends SpipPluginsListCommand
{

	protected function configure() {
		$this->setName("spip:plugins:desactivate")
			->setDescription("Désactive un ou plusieurs plugins")
			->addArgument('prefixes', InputArgument::OPTIONAL|InputArgument::IS_ARRAY, 'Liste des préfixes à désactiver')
			->addOption('all', 'a', InputOption::VALUE_OPTIONAL, 'Désactive tous les plugins actifs')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = new PolatoucheStyle($input, $output);
		$this->io->title("Activer des plugins");

		$spip = $this->getService('spip.loader');
		$spip->load();
		$this->actualiserPlugins();

		$prefixes = $input->getArgument('prefixes');
		if ($input->getOption('all')) {
			$prefixes = array_keys($this->getPluginsActifs(['procure' => false, 'php' => false]));
			$prefixes = array_map('strtolower', $prefixes);
		}


		if (!$prefixes) {
			$this->io->care("Aucun préfixe à désactiver");
			return;
		}

		$this->io->text("Liste des plugins à désactiver :");
		$this->presenterListe($prefixes);

		$this->desactivePlugins($prefixes);
	}


	public function desactivePlugins($prefixes) {
		if (!count($prefixes)) {
			$this->io->care("Aucun prefixe à désactiver");
			return true;
		}
		$actifs = array_keys($this->getPluginsActifs());
		$actifs = array_map('strtolower', $actifs);

		if ($deja = array_diff($prefixes, $actifs)) {
			$prefixes = array_diff($prefixes, $deja);
			if ($prefixes) {
				$this->io->text("Certains préfixes demandés sont déjà inactifs :");
				$this->presenterListe($deja);
			} else {
				$this->io->check("Tous les préfixes demandés sont déjà inactifs");
				return true;
			}
		}

		$desactiver = [];
		foreach ($this->getPluginsActifs() as $plugin) {
			$prefixe = strtolower($plugin['nom']);
			if (in_array($prefixe, $prefixes)) {
				$desactiver[] = $plugin['dir'];
				$prefixes = array_diff($prefixes, [$prefixe]);
			}
		}

		ecrire_plugin_actifs($desactiver, false, 'enleve');
		$actifs = $this->getPluginsActifs(['procure' => false, 'php' => false]);
		$this->io->text("Plugins actifs après action :");
		$this->showPlugins($actifs);
	}

}