<?php

namespace Polatouche\Plugin;

use Pimple\ServiceProviderInterface;

interface PluginProviderInterface extends ServiceProviderInterface {
	/** Only need register() */
}
