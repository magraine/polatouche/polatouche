<?php

namespace Polatouche\Provider\Spip;

use Pimple\Container;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Polatouche\Tools\Files;


/**
 * Chargement de SPIP
 * @api
 */
class Loader {

	/** @var string Chemin du démarreur */
	protected $starter = 'ecrire/inc_version.php';

	/** @var string Chemin du connecteur SQL */
	protected $connect = 'config/connect.php';

	/** @var string */
	private $directory;

	/** @var bool */
	private $loaded = false;

	/** @var bool */
	private $exists;

	/** @var Container */
	private $app;

	/** @var EventDispatcherInterface */
	private $dispatcher;



	/**
	 * Loader constructor.
	 * @param null $directory
	 */
	public function __construct($directory = null) {
		if (is_null($directory)) {
			$directory = getcwd();
		}
		$this->directory = rtrim(Files::formatPath($directory), DIRECTORY_SEPARATOR);
	}

	/**
	 * Indique si on est à la racine d’un site SPIP
	 * @return bool;
	 */
	public function exists() {
		if (is_null($this->exists)) {
			$this->exists = is_file($this->getPathFile($this->starter));
		}
		return $this->exists;
	}

	/**
	 * Démarre SPIP
	 */
	public function load() {
		if ($this->loaded) {
			return true;
		}

		if (!$this->exists()) {
			throw new \Exception('SPIP has not been found in ' . $this->directory);
		}

		$starter = $this->getPathFile($this->starter);
		$this->discoverDatabase();
		$this->loaded = true;
		$this->runSpipWithUglyGlobals($starter);

		if (!defined('_ECRIRE_INC_VERSION')) {
			throw new \Exception('SPIP is incorrectly loaded');
		}

		// Charger l'API SQL.
		include_spip('base/abstract_sql');

		return true;
	}

	/**
	 * Vérifie si SPIP est installé (bdd valide)
	 */
	public function discoverDatabase() {
		$connect = $this->getPathFile($this->connect);
		if (!is_file($connect)) {
			throw new \Exception('SPIP database is not configured');
		}
		$this->app['spip.sql'] = new Sql($connect);
	}

	/**
	 * Retourne un chemin complet vers un fichier de SPIP
	 * @param string $path Chemin tel que 'ecrire/inc_version.php'
	 * @return string Chemin complet
	 */
	function getPathFile($path) {
		return $this->directory . DIRECTORY_SEPARATOR . Files::formatPath($path);
	}

	/**
	 * Déclarer les globales utilisées encore par SPIP.
	 * @param string $starter Chemin du fichier de démarrage de SPIP.
	 */
	public function runSpipWithUglyGlobals($starter) {
		global
			$nombre_de_logs,
			$taille_des_logs,
			$table_prefix,
			$cookie_prefix,
			$dossier_squelettes,
			$filtrer_javascript,
			$type_urls,
			$debut_date_publication,
			$ip,
			$mysql_rappel_connexion,
			$mysql_rappel_nom_base,
			$test_i18n,
			$ignore_auth_http,
			$ignore_remote_user,
			$derniere_modif_invalide,
			$quota_cache,
			$home_server,
			$help_server,
			$url_glossaire_externe,
			$tex_server,
			$traiter_math,
			$xhtml,
			$xml_indent,
			$source_vignettes,
			$formats_logos,
			$controler_dates_rss,
			$spip_pipeline,
			$spip_matrice,
			$plugins,
			$surcharges,
			$exceptions_des_tables,
			$tables_principales,
			$table_des_tables,
			$tables_auxiliaires,
			$table_primary,
			$table_date,
			$table_titre,
			$tables_jointures,
			$liste_des_statuts,
			$liste_des_etats,
			$liste_des_authentifications,
			$spip_version_branche,
			$spip_version_code,
			$spip_version_base,
			$spip_sql_version,
			$spip_version_affichee,
			$visiteur_session,
			$auteur_session,
			$connect_statut,
			$connect_toutes_rubriques,
			$hash_recherche,
			$hash_recherche_strict,
			$ldap_present,
			$meta,
			$connect_id_rubrique,
			$puce;

		// Éviter des notices. Il faudrait utiliser HTTPFondation\Request dans SPIP.
		if (!$this->app['debug']) {
			foreach (['SERVER_NAME', 'SERVER_PORT', 'REQUEST_METHOD', 'REQUEST_URI'] as $key) {
				if (!isset($_SERVER[$key])) {
					$_SERVER[$key] = null;
				}
			}
		}

		require_once $starter;
	}

	/**
	 * Chemin vers le répertoire SPIP
	 * @return string
	 */
	public function getDirectory() {
		return $this->directory;
	}


	/**
	 * Sets a pimple instance onto this application.
	 *
	 * @param Container $app
	 * @return void
	 */
	public function setContainer(Container $app) {
		$this->app = $app;
	}

	public function setDispatcher(EventDispatcherInterface $dispatcher)  {
		$this->dispatcher = $dispatcher;
	}
}