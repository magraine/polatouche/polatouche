<?php

namespace Polatouche\Provider\Console;

use Cilex\Provider\Console\Command as BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\Translator;
use Polatouche\Provider\Console\Style\PolatoucheStyle;

/**
 * Base class for Polatouche commands.
 * @api
 */
abstract class Command extends BaseCommand {

	/** @var PolatoucheStyle */
	protected $io;

	/**
	 * {@inheritdoc}
	 * @note Juste pour l’IDE qui ralait sur le `@return` de Cilex pas assez permissif
	 * @return mixed|null
	 */
	public function getService($name) {
		return parent::getService($name);
	}

	/**
	 * Crée une entrée sortie avec du style
	 * @note Juste un raccourcis d’écriture.
	 * @return PolatoucheStyle
	 */
	public function getIO(InputInterface $input, OutputInterface $output) {
		$io = $this->getService('console.io');
		return $io($input, $output);
	}

	/**
	 * Traduire (helper)
	 * @return string
	 */
	public function trans($id, array $parameters = array(), $domain = null, $locale = null) {
		/** @var Translator $translator */
		$translator = $this->getService('translator');
		return $translator->trans($id, $parameters, $domain, $locale);
	}

	/**
	 * Transforme un tableau en tableau de n colonnes
	 *
	 * Utile pour présenter une longue liste sur l’écran
	 *
	 *     $liste = Command::columns($liste, 6, true);
	 *     $io->table([], $liste);
	 *
	 * @param array $list Le tableau unidimensionel
	 * @param int $columns Le nombre de colonnes souhaitées
	 * @param bool $flip Change l’ordre des éléments
	 *     Si A, B, C sont les premières entrées du tableau $array
	 *     - false : A, B, C sera en première ligne,
	 *     - true : A, B, C sera en première colonne
	 * @return array[]
	 */
	static function columns(array $list, $columns = 6, $flip = false) {
		$nb = count($list);
		if ($mod = $nb % $columns) {
			$list = array_pad($list, $nb + $columns - $mod, null);
		}
		$list = array_chunk($list, $columns);
		if ($flip) {
			$list = self::flip($list);
		}
		return $list;
	}


	/**
	 * Flip bidimensional array
	 * @link https://stackoverflow.com/questions/2221476/php-how-to-flip-the-rows-and-columns-of-a-2d-array
	 */
	static public function flip($arr) {

		$rows = count($arr);
		$ridx = 0;
		$cidx = 0;

		$out = array();

		foreach($arr as $rowidx => $row){
			foreach($row as $colidx => $val){
				$out[$ridx][$cidx] = $val;
				$ridx++;
				if($ridx >= $rows){
					$cidx++;
					$ridx = 0;
				}
			}
		}
		return $out;
	}
}