<?php

namespace Polatouche\Provider;

use Pimple\Container;

/**
 * Spip Service Provider
 */
class SpipServiceProvider implements \Pimple\ServiceProviderInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function register(Container $app)
	{
		$app['spip.loader'] = function ($app) {
			$spip = new Spip\Loader($app['spip.directory']);
			$spip->setContainer($app);
			$spip->setDispatcher($app['dispatcher']);
			return $spip;
		};

		$config = [
			'spip.webmestre.email' => '',
			'spip.webmestre.login' => '',
			'spip.webmestre.nom' => '',
			'spip.webmestre.login.prefixe' => 'Polatouche-',
		];

		foreach ($config as $key => $value) {
			if (!isset($app[$key])) {
				$app[$key] = $value;
			}
		}
	}
}
