<?php

namespace Polatouche;

use Cilex\Application as Cilex;
use Symfony\Component\Finder\Finder;
use Polatouche\Plugin\PluginProviderInterface;
use Pimple\ServiceProviderInterface;


class Application extends Cilex {

	const NAME = "Polatouche";
	const VERSION = "1.0.0-dev";

	const PLUGINS_DIR = "plugins/";

	/**
	 * Application constructor.
	 *
	 * Charge automatiquement les commandes présentes
	 * dans le répertoire Command de ce projet.
	 */
	public function __construct($options = []) {
		parent::__construct(self::NAME, self::VERSION);

		// todo: config.yaml file
		$this['locale'] = 'fr_FR';
		$this['debug'] = false;
		$this['spip.directory'] = getcwd();

		$this['polatouche.auto-register-plugins'] = true;
		$this['polatouche.auto-register-spip-cli'] = true;

		foreach ($options as $key => $value) {
			$this[$key] = $value;
		}

		$this->setTimezone();
		$this->registerServices();
		$this->registerTranslationsInProject(self::class);
		$this->registerCommandsInProject(self::class);

		// plugins/ chargés automatiquement
		if ($this['polatouche.auto-register-plugins']) {
			$this->autoRegisterPlugins();
		}

		// spip-cli/ chargés automatiquement
		if ($this['polatouche.auto-register-spip-cli']) {
			$this->autoRegisterSpipCli();
		}
	}

	/**
	 * Liste des services proposés par défaut
	 */
	protected function registerServices() {
		$this->register(new \Silex\Provider\TranslationServiceProvider(), array(
			'locale_fallbacks' => $this->fallbackLocale($this['locale'], 'en'),
		));
		$this['translator']->addLoader('php', new \Symfony\Component\Translation\Loader\PhpFileLoader());

		$this['console.io'] = function() {
			return function($input, $output) {
				return new \Polatouche\Provider\Console\Style\PolatoucheStyle($input, $output);
			};
		};

		// SPIP !
		$this->register(new Provider\SpipServiceProvider());
	}

	/**
	 * Ajoute les commandes contenues dans le répertoire Command du même chemin que la classe transmise
	 */
	protected function registerCommandsInProject($class) {
		foreach ($this->findCommandsInProject($class) as $command) {
			if (class_exists($command)) {
				$this->command(new $command());
			}
		}
	}

	/**
	 * Retourne la liste des commandes au même niveau que ce projet
	 * @return array
	 */
	public function findCommandsInProject($class) {
		$class = new \ReflectionClass($class);
		$commandDir = dirname($class->getFilename()) . '/Command'; // .../polatouche/src/Command
		$list = [];
		if (is_dir($commandDir)) {
			$namespace = $class->getNamespaceName(); // Polatouche ou Polatouche\Plugin\Nom
			$finder = new Finder();
			$finder->files()->in($commandDir)->name('*Command.php');
			foreach ($finder as $file) {
				// Polatouche\Command\Dir\NameCommand ou Polatouche\Plugin\Nom\Command\NameCommand
				$list[] = $namespace . '\\Command\\' . str_replace('/', '\\', substr($file->getRelativePathname(), 0, -4));
			}
		}
		return $list;
	}

	/**
	 * Déclare un plugin.
	 *
	 * Déclare un service et ajoute automatiquement ses commandes et traductions
	 */
	public function register(ServiceProviderInterface $provider, array $values = array()) {
		parent::register($provider, $values);
		if ($provider instanceof PluginProviderInterface) {
			$this->registerCommandsInProject($provider);
			$this->registerTranslationsInProject($provider);
		}
	}


	/**
	 * Charge les Traductions d’un projets (dans la locale souhaitée)
	 */
	public function registerTranslationsInProject($class) {
		$translator = $this['translator'];
		$class = new \ReflectionClass($class);
		$translationsDir = dirname($class->getFilename()) . '/Resources/translations';
		if (is_dir($translationsDir)) {
			$locales = array_unique(array_merge([$translator->getLocale()], $translator->getFallbackLocales()));
			foreach ($locales as $locale) {
				$finder = new Finder();
				$finder->files()->in($translationsDir)->name('*.' . $locale . '.php');
				foreach ($finder as $file) {
					//$domain = substr($file->getBasename(), 0, -strlen('.' . $locale . '.php'));
					$translator->addResource('php', $file->getPathname(), $locale);
				}
			}
		}
	}

	/**
	 * Explose un nom de locale en autant d’éléments valides en fallback
	 *
	 * @param string $locale Nom d’une locale tel que 'fr_FR'
	 * @param string $fallback Fallback en dernier recours à appliquer
	 * @return array Liste de locales
	 */
	public function fallbackLocale($locale, $fallback = 'en') {
		// fr_FR_XX => 4 langues : fr_FR_xx, fr_FR, fr, en (défaut)
		$locales = [$locale];
		$liste = explode('_', $locale);
		do {
			array_pop($liste);
			if ($liste) {
				$locales[] = implode('_', $liste);
			}
		} while ($liste);

		if ($fallback and ($fallback !== end($locales))) {
			$locales[] = $fallback;
		}
		return $locales;
	}

	/**
	 * Chargement automatique des plugins présents dans plugins/
	 *
	 * Fonctionne s’ils utilisent un nommage spécifique
	 * - composer.json => "type" : "polatouche-plugin",
	 * - fichier "src/{Xxx}Plugin.php" présent
	 */
	public function autoRegisterPlugins() {
		$root = Tools\Files::findAppRootDirectory();
		$dir = $root . self::PLUGINS_DIR;
		$plugins = (new Finder())->directories()->in(rtrim($dir, '/'))->depth(' == 0');
		foreach ($plugins as $dir) {
			$starters = (new Finder())->files()->in($dir . '/src')->depth('== 0')->name('*Plugin.php');
			foreach ($starters as $plugin) {
				$class = '\\' . $plugin->getBasename('.php');
				if ($namespace = Tools\Files::getNamespace($plugin->getPathname())) {
					$class = '\\' . $namespace . $class;
				}
				$this->register(new $class());
			}
		}
	}


	/**
	 * Chargement automatique des commandes spip-cli/ présentes dans les répertoires spip-cli/
	 */
	public function autoRegisterSpipCli() {
		// les commandes dans spip-cli/ de ce plugin (à migrer en commande v2/polatouche)
		$this->registerSpipCliBaseCommands(self::class);
		// les commandes dans spip-cli/ des plugins SPIP actifs
		$this->registerSpipCliPluginsCommands();
		// charger les vilaines globales de spip-cli
		try {
			global $spip_racine, $spip_loaded, $cwd;
			$cwd = getcwd();
			$spip = $this['spip.loader'];
			$spip_racine = $spip->getDirectory();
			$spip_loaded = $spip->load();
		} catch (\Exception $e) {
			return false;
		}
	}

	/**
	 * Chargement des commandes présentes dans 'spip-cli/' de ce plugin
	 *
	 * Ce sont des commandes qui n’ont pas été migrées dans la nouvelle écriture
	 * @return bool
	 */
	public function registerSpipCliBaseCommands($class) {
		$class = new \ReflectionClass($class);
		$spipCliDir = dirname($class->getFilename()) . '/spip-cli'; // .../polatouche/src/spip-cli

		$commands = (new Finder())->files()->in($spipCliDir)->depth(' == 0');
		foreach ($commands as $file) {
			$this->registerSpipCliCommand($file->getPathname());
		}
	}

	/**
	 * Chargement des commandes présentes dans 'spip-cli/' des plugins SPIP actifs.
	 *
	 * Nécessite de démarrer SPIP (et donc d’être dans un SPIP)
	 * @return bool
	 */
	public function registerSpipCliPluginsCommands() {
		try {
			$spip = $this['spip.loader'];
			if (!$spip->load()) {
				return false;
			}
		} catch (\Exception $e) {
			return false;
		}
		$commandes = find_all_in_path('spip-cli/', '.*[.]php$');
		foreach ($commandes as $path) {
			$this->registerSpipCliCommand($path);
		}
	}

	/**
	 * Déclare une commande Spip Cli
	 * @return bool
	 */
	public function registerSpipCliCommand($path) {
		$command = '\\' . basename($path, '.php');
		include_once($path);
		if ($namespace = Tools\Files::getNamespace($path)) {
			$command = '\\' . $namespace . $command;
		}
		if (class_exists($command)) {
			$this->command(new $command());
		}
	}

	/**
	 * If the timezone is not set anywhere, set it to UTC.
	 * @return void
	 */
	protected function setTimezone() {
		if (false == ini_get('date.timezone')) {
			date_default_timezone_set('UTC');
		}
	}
}